"""tails_api URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path
from django.conf.urls import url, include
from rest_framework import routers
from asset_mngment.asset_mngment import views
from asset_mngment import settings


router = routers.DefaultRouter()
router.register(r'assets', views.AssetViewSet)
router.register(r'asset-requests', views.AssetRequestViewSet)


urlpatterns = [
    path('admin/', admin.site.urls),
    url(r'^', include(router.urls)),
    url(r'^users/$', views.UserViewSet.as_view({'get':'list','post': 'create'})),
    url(r'^users/(?P<pk>[0-9]+)/$', views.UserViewSet.as_view({'get': 'retrieve'})),
]
