from django.shortcuts import render
from rest_framework import viewsets
from rest_framework import generics
import django_filters.rest_framework as filters
from rest_framework.response import Response
from django.contrib.auth.models import User
from asset_mngment.asset_mngment.models import Asset, AssetRequests
from asset_mngment.asset_mngment.serializers import UserSerializer, \
    AssetSerializer, AssetRequestsSerializer, AssetListSerializer, \
    AssetRequestsListSerializer

class UserViewSet(viewsets.ModelViewSet):
    queryset = User.objects.all()
    serializer_class = UserSerializer
    filter_backends = (filters.DjangoFilterBackend,)
    filter_fields = ('username',)

class AssetViewSet(viewsets.ModelViewSet):
    queryset = Asset.objects.all()
    serializer_class = AssetSerializer
    # filter_backends = (filters.SearchFilter,)
    # filter_fields = ('name', 'details')
    def list(self, request):
        queryset = Asset.objects.all()
        serializer_class = AssetListSerializer(queryset, many = True)
        # filter_backends = (filters.DjangoFilterBackend,)
        # filter_fields = ('name', 'details')
        # print(serializer_class)
        print("check type {0}".format(type(serializer_class)))
        return Response(serializer_class.data)

class AssetRequestViewSet(viewsets.ModelViewSet):
    queryset = AssetRequests.objects.all()
    serializer_class = AssetRequestsSerializer
    filter_backends = (filters.DjangoFilterBackend,)
    filter_fields = ('is_approved',)
    def list(self, request):
        queryset = AssetRequests.objects.all()
        serializer_class = AssetRequestsListSerializer(queryset, many = True)
        filter_backends = (filters.DjangoFilterBackend,)
        filter_fields = ('is_approved',)
        # print(serializer_class)
        return Response(serializer_class.data)
