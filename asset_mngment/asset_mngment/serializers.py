from rest_framework import serializers
from django.contrib.auth.models import User
from asset_mngment.asset_mngment.models import Asset, AssetRequests

class UserSerializer(serializers.ModelSerializer):

    class Meta:
        model = User
        fields = ('id', 'username', 'password')


class UserSerializer2(serializers.ModelSerializer):

    class Meta:
        model = User
        fields = ('username',)


class AssetSerializer(serializers.ModelSerializer):
    # owner = UserSerializer()
    class Meta:
        model = Asset
        fields = '__all__'


class AssetListSerializer(serializers.ModelSerializer):
    username = serializers.ReadOnlyField(source = 'owner.username')
    class Meta:
        model = Asset
        fields = ('id', 'created', 'owner', 'name', 'image_url', 'details', 'username')


class AssetRequestsSerializer(serializers.ModelSerializer):
    # user = UserSerializer()
    # asset = AssetSerializer()
    class Meta:
        model = AssetRequests
        fields = '__all__'


class AssetRequestsListSerializer(serializers.ModelSerializer):
    user = UserSerializer()
    asset = AssetSerializer()
    class Meta:
        model = AssetRequests
        fields = '__all__'
