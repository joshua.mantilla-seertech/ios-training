# Generated by Django 2.0.7 on 2018-07-30 09:36

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('asset_mngment', '0007_auto_20180730_0852'),
    ]

    operations = [
        migrations.AlterField(
            model_name='asset',
            name='image_url',
            field=models.TextField(blank=True, default=''),
        ),
    ]
