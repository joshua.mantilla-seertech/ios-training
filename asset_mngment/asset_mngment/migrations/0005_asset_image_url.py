# Generated by Django 2.0.7 on 2018-07-30 08:46

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('asset_mngment', '0004_auto_20180722_1353'),
    ]

    operations = [
        migrations.AddField(
            model_name='asset',
            name='image_url',
            field=models.CharField(blank=True, default='', max_length=500),
        ),
    ]
