# from django.conf.urls import url
# from rest_framework.urlpatterns import format_suffix_patterns
# from asset_mngment.views import UserViewSet, AssetViewSet
# from rest_framework import renderers
#
# asset_list = AssetViewSet.as_view({
#     'get': 'list',
#     'post': 'create'
# })
#
# asset_detail = AssetViewSet.as_view({
#     'get': 'retrieve',
#     'put': 'update',
#     'patch': 'partial_update',
#     'delete': 'destroy'
# })
#
#
# user_list = UserViewSet.as_view({
#     'get': 'list'
# })
#
# user_detail = UserViewSet.as_view({
#     'get': 'retrieve'
# })
#
# urlpatterns = format_suffix_patterns([
#     url(r'^assets/$', asset_list, name='asset-list'),
#     url(r'^assets/(?P<pk>[0-9]+)/$', asset_detail, name='asset-detail'),
#     url(r'^users/$', user_list, name='user-list'),
#     url(r'^users/(?P<pk>[0-9]+)/$', user_detail, name='user-detail')
# ])
