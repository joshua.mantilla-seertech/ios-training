from django.db import models
from django.contrib.auth.models import User
# Create your models here.
class Asset(models.Model):
    created = models.DateTimeField(auto_now_add=True)
    owner = models.ForeignKey(User, related_name='assets', on_delete=models.CASCADE)
    image_url = models.TextField(blank = True, default = '')
    name = models.CharField(max_length = 100, blank = True, default='')
    details = models.TextField()

    # def save(self, *args, **kwargs):
    #     """
    #     Use the `pygments` library to create a highlighted HTML
    #     representation of the code snippet.
    #     """
    #     lexer = get_lexer_by_name(self.language)
    #     linenos = 'table' if self.linenos else False
    #     options = {'title': self.title} if self.title else {}
    #     formatter = HtmlFormatter(style=self.style, linenos=linenos,
    #                               full=True, **options)
    #     self.highlighted = highlight(self.code, lexer, formatter)
    #     super(Snippet, self).save(*args, **kwargs)

    class Meta:
        ordering = ('created',)


class AssetRequests(models.Model):
    created = models.DateTimeField(auto_now_add=True)
    user = models.ForeignKey(User, related_name='assets_request_user', on_delete=models.CASCADE)
    asset = models.ForeignKey(Asset, related_name='assets_request_asset', on_delete=models.CASCADE)
    is_approved = models.BooleanField( default = False )

    class Meta:
        ordering = ('created',)
